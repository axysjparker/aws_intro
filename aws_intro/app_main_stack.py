from os import path
from aws_cdk import core

import aws_cdk.aws_lambda as lmb
import aws_cdk.aws_apigateway as apigw
import aws_cdk.aws_sns as sns

class MainStack(core.Stack):

    def __init__(self, scope: core.Construct, id: str, **kwargs) -> None:
        super().__init__(scope, id, **kwargs)

        this_dir = path.dirname(__file__)
        
        # Define the outbound SNS topic
        demo_topic = sns.Topic(self, 
            id = 'DemoSNSTopic',
            display_name = 'NewDataAvail'
            )

        # Define the new lambda function:
        demo_lambda = lmb.Function(self, 
            # the name of the function in the console
            id = 'DemoIngestLambda',
            # the code asset to deploy
            code=lmb.Code.from_asset(path.join(this_dir, 'lambdas', 'demo_ingest')),
            # the python module and function to call when the lambda is triggered (from the path above)
            handler='demo.lambda_handler',
            # Set the lambda runtime
            runtime=lmb.Runtime.PYTHON_3_7,
            # Pass through a map for environment variables:
            environment = {
                'SNS_TOPIC': demo_topic.topic_arn
            }
        )

        # Give the lambda function publish access to the topic:
        demo_topic.grant_publish(demo_lambda)

        # Define an API to hit the lambda from the internet:
        demo_gateway = apigw.LambdaRestApi(self, 
            id = 'DemoLambdaGW',
            # This tells gateway to use the current version of the code for this lambda function:
            handler = demo_lambda.current_version,
            description = 'Creates an endpoint for the DemoIngestLambda accessible from the web'
        )

        # Finally, store the url of the gateway for use later if needed.
        self.url_output = core.CfnOutput(self, 'DemoLambdaUrl', value = demo_gateway.url)