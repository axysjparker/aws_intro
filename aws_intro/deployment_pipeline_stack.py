import aws_intro.app_config as cfg

from aws_cdk import core
from aws_cdk import pipelines
from aws_cdk import aws_codepipeline as codepipeline
from aws_cdk import aws_codepipeline_actions as cpactions

from .app_main_stack import MainStack

class DeploymentStage(core.Stage):

    def __init__(self, scope: core.Construct, id: str, **kwargs) -> None:
        super().__init__(scope, id, **kwargs)

        main_stack = MainStack(self, 'workshop')


class DeployPipelineStack(core.Stack):

    def __init__(self, scope: core.Construct, id: str, **kwargs):
        super().__init__(scope, id, **kwargs)

        source_artifact = codepipeline.Artifact()
        cloud_assembly_artifact = codepipeline.Artifact()

        pipeline = pipelines.CdkPipeline(self, f'{cfg.user}-WorkshopPipeline',
            cloud_assembly_artifact=cloud_assembly_artifact,
            pipeline_name=f'{cfg.user}-Workshop',

            source_action=cpactions.BitBucketSourceAction(
                action_name='Bitbucket',
                output=source_artifact,
                connection_arn=cfg.bitbucket_connection_arn,
                owner=cfg.repo_owner,
                repo=cfg.repo,
                branch=cfg.repo_branch),
                
            synth_action=pipelines.SimpleSynthAction(
                source_artifact=source_artifact,
                cloud_assembly_artifact=cloud_assembly_artifact,
                install_command='npm install -g aws-cdk && pip install -r requirements.txt',
                synth_command='cdk synth')
        )

        # Uncomment this after the initial deploy is completed in CDK!
        pipeline.add_application_stage(DeploymentStage(self, cfg.user, env=cfg.sandbox_env))
