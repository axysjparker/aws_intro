import os
import boto3

# the lambda event handler function should only handle processing the event dictionary 
# and then pass it on to a seperate, more easily unit testable function.

SNS_TOPIC = os.environ.get('SNS_TOPIC')
sns_topic = boto3.resource('sns').Topic(SNS_TOPIC)

def lambda_handler(event, context):
    # Different services have different event structures, this is only valid for API Gateway:
    print(f'The inbound request: {json.dumps(event)}')

    return do_work_here(event['path'])

def do_work_here(path):
    resp = sns_topic.publish(
        Message = f'Hello World!'
    )

    print(f'Find this in CloudWatch Logs: {path}. Sent as message id {resp["MessageId"]}')

    # The 200 status code is also what tells lambda the function finished successfully
    # not just for a http rest api
    return {
        'body': f'Hello World! received at {path}',
        'headers': {
            'Content-Type': 'text/plain'
        },
        'statusCode': '200'
    }