#!/usr/bin/env python3

from aws_cdk import core

from aws_intro.deployment_pipeline_stack import DeployPipelineStack
from aws_intro import app_config as cfg

app = core.App()
DeployPipelineStack(app, f'{cfg.user}-WorkshopPipeline', env = cfg.sandbox_env)

app.synth()
