# Task 6: Ingesting from Email

This exercise is more about researching the implenetation of functionality we haven't tried. Some buoys in the fleet communicate data using IDP, which relays messages to the data pipeline over Email.

Amazon SES is a service that allows the ingestion of emails into a normal workflow, and this task is about figuring out how to ingest and receive an email using SES, triggering a Lambda Function from that email, and processing its data including any potential attachments.

![task6](assets/task6.png)