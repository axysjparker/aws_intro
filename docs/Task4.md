# Task 4: More Complicated Step Functions

This task will involve reworking the flow of the state machine to use error handling instead of switch statements for the ".bad" input file. We are also going to move the logic of how to handle files/nofiles into seperate lambda functions, as shown here:

![task4](assets/task4.png)

The new starting point for the state machine is a Lambda Function that performs some sort of check on the data to determine if the input is "bad". If it is, the function should error, which should move the state machine to an error state.

After this lambda, a new Choice state will send execution to one of 3 new lambda functions. For inputs with no file data, create an output that includes a timestamp, and some arbitrary data as secondary information. For inputs with JSON files or data, send them to a function that will take the file and create a dictionary that includes a timestamp along with the json data as the function output. Both of these cases should lead to a "Store in DynamoDB" step function task. This task should write the output to the dynamodb table - do not use boto3 inside the lambda function this time.

A seperate lambda function should handle all other file times as was done before, writing them directly to a second S3 bucket after some modification.

Both execution paths should converge back on the Send Data Notification state and then finish execution.

## Resources
