# Task 1: Adding a Lambda Function and I/O

In the currently deployed application, API Gateway triggers an ingest lambda function which then sends a message on a Simple Notification Service (SNS) messaging topic. For this task, you will first add a lambda function that writes the SNS message received to a S3 bucket created by the CDK infrastructure, as shown here:

![task1 goal](assets/task1.png)

An approach to this would be:

1. Create the output S3 Bucket in CDK.
2. Create a new Lambda Function that writes the contents of its Event to an object in S3 (boto3 API), making sure to have unique file names if you don't want to overwrite data.
3. Set up the new Lambda Function to trigger (subscribe) to the SNS topic provided.

Once completed, you should be able to send a request to the API Gateway endpoint from Task 0 and each time you do a new object should be created in S3. You should verify this in the S3 Console, and check the CloudWatch execution logs for the function. To view the logs of a functions execution, open the AWS Lambda Console and find the relevant function. Choose the monitoring page, and hit the link for "View Logs in CloudWatch". Each execution will generate a new log entry, but be aware that updates to the code or executions not on the same lambda instance will show up in different log streams. AWS provides more detail on this [here](https://docs.aws.amazon.com/lambda/latest/dg/monitoring-cloudwatchlogs.html).

As a note, writing small sizes of data to S3 is typically inefficient (cost and performance) and should usually avoided in favour of ElastiCache or DynamoDb. It's used here to gain some familiarity with S3. Additionally, SNS messages can only work with data up to 256KB (which is actually billed as 4 messages of 64KB each). So if we want to support ingesting a file or larger data from API Gateway, we need the ingest function to store the file in a temporary location, and pass the location of the stored object on a SNS topic for another function to read and perform actions on.

## Task 1.5: Ingesting Files and "Transforming" Them

**Note:** A best practice is to have the handler of your lambda function (`lambda_handler(event, context)`) only perform the work necessary to handle the event. For example, with a function that reads data from an S3 object - described by the input event - and then transforms it, the lambda_handler should only extract the bucket name and object key from the event before calling a helper function to perform any logic or actions on it.

To extend on the above system, we will alter the application to accept a file being posted onto the API Gateway URL, which requires some changes to the above:

![task1.5 goal](assets/task1.5.png)

In this system, Ingest Lambda will look at the API Gateway event and store the file data it contains in a temporary S3 bucket. The Ingest function will then be modified to send the SNS message with the S3 Object location. The function that subscribes to it should then read this file from S3, transform it, and write it to a second bucket. For this demo, the transform could be as simple as changing the file name before re-upload.

## Outcome

At the end of these tasks the system should:

* Accept a GET or POST request to the API Gateway Endpoint
* If the POST request contains file data, save it to a temporary location and send an SNS topic.
* If the request contains no file data or is a GET, send a random SNS message.
* The Transform Data function should:
    * If an object key is provided in the event, read it, alter it in some way, and store it in a second S3 Bucket
    * If no object key is provided, store the actual event itself in JSON form in the second S3 Bucket

## Resources

Before getting started, I would recommend reviewing these two pages:

* [Working with the CDK in Python](https://docs.aws.amazon.com/cdk/latest/guide/work-with-cdk-python.html)
* [Translating CDK Examples Written in Typescript](https://docs.aws.amazon.com/cdk/latest/guide/multiple_languages.html)

Some more general, but useful documentation is available here:

* [API Gateway Lambda Events](https://docs.aws.amazon.com/lambda/latest/dg/services-apigateway.html)
* [Boto3 S3 API](https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/s3.html)
* [CDK: Lambda Event Sources](https://docs.aws.amazon.com/cdk/api/latest/python/aws_cdk.aws_lambda_event_sources.html)
* [CDK: Lambda Functions](https://docs.aws.amazon.com/cdk/api/latest/python/aws_cdk.aws_lambda/Function.html)
* [CDK: S3 Buckets](https://docs.aws.amazon.com/cdk/api/latest/python/aws_cdk.aws_s3/Bucket.html)
* [CDK: SNS Subscriptions](https://docs.aws.amazon.com/cdk/api/latest/python/aws_cdk.aws_sns_subscriptions.html)
* [CDK: SNS Topics](https://docs.aws.amazon.com/cdk/api/latest/python/aws_cdk.aws_sns.html)