# Task 5: Ingest from a Second Source (SQS Queue)

Next we add the ability to ingest from different event sources, in this case S3 object uploads, and SQS Queue Messages, as shown here:

![task5](assets/task5.png)


## SQS Messages

Implement an SQS Queue in CDK that triggers the Ingest Lambda function. This will require altering the ingest function to support two different event dictionary structures. The SQS queue will receive a jsonstring that can be parsed as a dictionary and passed into the input of the step function workflow, and will not have to deal with files at all.

## S3 Object Triggers

It is possible to trigger a Step Function Workflow from an S3 Object event directly. If time allows, change the ingest function to not trigger the step function execution directly using boto3, and instead upload the file to the S3 bucket and have that trigger the step function execution. (SQS and non-file api hits will still trigger it with boto3). If this change is made, the workflow should be able to be executed by manually uploading a file to the S3 bucket either from the CLI or AWS console.
