# Task 3: Adding DynamoDB

DynamoDB is a NoSQL database system managed by AWS. It provides very low and predictable latency (which can be sub 10 millisecond) and a near infinite scalability. It's worth reading about the [different modes](https://aws.amazon.com/dynamodb/pricing/) dynamodb can be configured in and their pricing models, but use **on-demand mode** for this workshop.

This task encompasses a relatively small change. First, use the CDK to create and deploy a DynamoDB table that has a single primary key called timestamp. Next, pass that table name into the Transform Data function as an environment variable.

Then, using Boto3, write an entry to the table on each call to Transform Data as described in [outcomes](#Outcomes).

![task3](assets/task3.png)

## Outcomes

Same as Task2, except:

* When no file is provided, write an entry in the DynamoDB table with a primary key of the current time, and a mapping that is the input event to the transform data function.
* When a file is provided, write an entry in the DynamoDB table with a primary key of the current time, and a mapping that has the object key and bucket name of the file being stored in s3 Bucket 2.
