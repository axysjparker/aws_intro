# Task 2: Creating a State Machine

One problem with Lambda Functions is that it can become difficult to keep track of what triggers what, and handling exceptions when they occur. AWS Step Functions provides a way to define state machines that control the flow of serverless applications.

To get started with them, you will modify the application from task 1 to utilize a standard workflow step function as shown here:

![task2](assets/task2.png)

In the revised system, the API Gateway triggers the ingest system and stores a file as before (if there is one), but instead of publishing a SNS message it will now start a Step Function execution (use boto3). 

The transform data function has the same functionality as before but the input event structure will change with the use of Step Functions and needs to be modified. See [Outcomes](##Outcomes) for how to structure it.

To make things easier, the CDK provides a collection of [step function tasks](https://docs.aws.amazon.com/cdk/api/latest/python/aws_cdk.aws_stepfunctions_tasks.html) which allow direct interaciton with AWS services (such as publishing a SNS message or invoking a lambda function).

### Invoking a Lambda Function as a State in a State Machine

Assuming you have defined the lambda function already as the variable `lambda1`:

```python
import aws_cdk.aws_stepfunctions_tasks as tasks
tasks.LambdaInvoke(self, 'Lambda 1',
            lambda_function=lambda1,
            output_path='$.Payload')
```

Of note is the `output_path`. By setting this to `$.Payload`, you can return a dictionary from the `lambda_handler` function in Lambda 1 which will be automatically passed to the input of the next state.

The SNS Topic is now moved to the last part of the workflow, and is there only to allow other systems to subscribe to data in the future (not implemented in this workshop). It should be implemented using the `SnsPublish` task, and specifying a input JSON Path payload.

The `resources/send_to_api.py` script can be used to send files to the api:

```bash
python resources/send_to_api.py API_ENDPOINT_URL NUMBER_OF_MESSAGES TIME_BETWEEN_MESSAGE [FILENAME(s) TO SEND]
```

## Task 2.5: Conditions and Branching

To make this state machine more useful, add a choice state that will send "bad" files to a fail state and halt execution (make the decision based on ".bad" as a file extension). Other requests are handled normally:

![task2.5](assets/task2.5.png)

## Outcomes

At the end of these tasks the system should:

* Accept a GET or POST request to the API Gateway Endpoint, which trigger a lambda function:
    * If the request contains no file data or is a GET, start a step function execution with the input:

        ```json
        {
            "object_key": "",
            "bucket_name": "",
            "datetime": "CURRENTTIME",
            "path": "REQUESTPATH"
        }
        ```

    * If the POST request contains file data, save it to a temporary location in S3, and start a step function execution with the input:

        ```json
        {
            "object_key": "TEMP_OBJECT/KEY.EXTENSION",
            "bucket_name": "TEMP_BUCKET_NAME",
            "datetime": "CURRENTTIME",
            "path": "REQUESTPATH"
        }
        ```
* The state machine should start at a choice state:
    * If the file extension is ".bad", go to a fail state.
    * All other files, or no-file should go to the transform data function
* The Transform Data function should:
    * If an object key is provided in the event, read it, alter it in some way, and store it in a second S3 Bucket
    * If no object key is provided, store the actual event itself in JSON form in the second S3 Bucket
    * Return the same dictionaries as above (pointing to the new bucket) for the next state.
* Publish an SNS message with the returned output of the Transform Data function.

## Step Functions in the AWS Console

Before starting, head to the AWS Console and open Step Functions. On this page you will see a few existing state machines, and when you open one it will display its most recent executions and other info:

![sfn1](assets/screens/sfn1.png)

If you open an execution, you are able to see a graphical view of the path it took through the machine. You can also click on a state in the graph to view more detail about it, including the step inputs and outputs, the Lambda execution logs in Cloudwatch Logs, and any exceptions that happened if any:

![sfn2](assets/screens/sfn2.png)

## Resources

When working with state machines in AWS, state Input and Output paths are built using JSON Paths. For info see the [Amazon States Language] page in the developer guide.

* [CDK: Step Functions](https://docs.aws.amazon.com/cdk/api/latest/python/aws_cdk.aws_stepfunctions.html)
* [CDK: Step Function Tasks](https://docs.aws.amazon.com/cdk/api/latest/python/aws_cdk.aws_stepfunctions_tasks.html)