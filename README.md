# AWS Serverless Introduction

This repo provides an introduction to building serverless applications in AWS. The objective is to provide exporsure to different services available and learn how they can be connected together (and defined using IaC).

If you have questions, get stuck, or are not sure how to proceed, reach out and ask!

## Prerequisites

The preferred environment for development is Python 3.7.5 on Linux. Windows users should utilize the Windows Subsystem for Linux (WSL)
Development work for this project is best done in a Linux environment to match the majority of AWS services. Windows users can enable the Windows Subsystem for Linux and Visual Studio Code to have a Linux based development environment on their local machine. A guide for installing WSL is [here](https://docs.microsoft.com/en-us/windows/wsl/install-win10) (note AXYS can only use WSL1, WSL2 requires a newer windows version), and a guide to configure VS Code is [here](https://code.visualstudio.com/docs/remote/wsl).

The Cloud Project uses Python 3.7 (3.7.5), so ensure that it is installed:

```bash
sudo apt update
sudo apt upgrade -y
sudo apt-get install software-properties-common
sudo add-apt-repository -y ppa:deadsnakes/ppa
sudo apt install -y python3 python3-pip python3.7 python3.7-venv unzip curl
```

You can also set python3.7 as the default python3 if wanted:

```bash
sudo update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.7 1
python3 --version
```

Next, install the AWS CLI V2, and login to retrieve credentials for using it:

```bash
mkdir ~/aws-cli .
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install

aws configure sso
```
When going through the SSO Configure, set the following values:

* SSO Start URL: https://axys.awsapps.com/start#/
* AWS Region: us-west-2

The terminal should open a browser and ask you to authenticate with the AXYS Active Directory (or if you have, redirect you a lot automatically), and then take you to a page with a "Sign in to AWS CLI." Once approved, select `"Axys Sandbox, axysaws-sandbox@axys.com (ACCOUNTID)` if asked for an account, and choose the AxysCoop role if prompted. Verify the region is us-west-2, set output format to `json`, and then name the profile `sandbox`. Finally set the environment variable `AWS_PROFILE=sandbox` to enable the credentials.

Finally, install NodeJS and the AWS Cloud Development Kit:

```bash
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
nvm --version
# If you get a "Commnand not found" or similar, close your terminal and reopen it before continuing

nvm install --lts

npm install -g aws-cdk
cdk --version
```

To simplify file access, create a folder in C for all development work (ex: C:\axys). Next we create a symlink to it in our home direcotry in WSL:
```bash
ln -s /mnt/c/axys ~/axys
```

## Workshop Overview

The objective of this workshop is to create a system for ingesting data from a number of sources, processing it, and storing it in different places. It is broken into 7 tasks which focus on building the functionality in stages.

An overview of the system being built is shown below:
![serverless workshop infrastructure](docs/assets/overview.png)

The system should be able to ingest different data types from multiple different sources (such as a message queue, api, or email), perform some data processing and detection to figure out what the input data was, store it in an appropriate location, and notify a listener that data is now stored. Hopefully, it should cover at least these services and practices:

* Amazon SQS
* Amazon SNS
* Amazon S3
* Amazon DynamoDB
* Amazon API Gateway
* Amazon Key Management Service (KMS)
* Amazon CloudWatch
* Amazon CodePipeline
* AWS CloudFormation
* AWS Identity and Access Management (IAM)
* AWS Step Functions (SFN)
* AWS Lambda
* The AWS Console
* The AWS CLI
* The AWS Cloud Development Kit
* Python Boto3 Library
* Using Amazon CloudWatch

This mini-pipeline uses the AWS CDK for managing all the infrastructure deployed, and will provide some tidbits and references on each task. A starting point skeleton code is provided and setup in Task 0.

## Brief introduction to the AWS CDK

The Cloud Development Kit is a tool for modelling and provisioning AWS infrastructure. The CDK is natively written in Typescript, but supports a few other languages including Python.

The base of everything in the CDK is the [construct](https://docs.aws.amazon.com/cdk/latest/guide/constructs.html), which can either be a single service, groupings of them, or higher level resources such as an entire CloudFormation stack. When working with the CDK, we create constructs based on how we want our infrastructure to talk together and be deployed, and the CDK then synthesizes our code into deployable CloudFormation templates which control the actual resources on AWS:

![aws cdk stack overview](https://docs.aws.amazon.com/cdk/latest/guide/images/AppStacks.png)

The official [AWS CDK Getting Started page](https://docs.aws.amazon.com/cdk/latest/guide/getting_started.html) provides a much better introduction to it than I can here.

## Task 0: AWS Cloud Development Kit Overview

To get started clone the repo and start a working branch off of master.

``` bash
git clone #REPOURL
cd aws_intro
git checkout -b username\workshop master
```

Next, setup the dev environment for the repo:

```bash
python3.7 -m venv .env
source .env/bin/activate
pip install wheel
pip install -r requirements.txt
```

The requirements.txt installs most required modules for development, including the AWS Data Wrangler, boto3, pytest, and parts of the AWS CDK.

The code provided is a starting point for an app built with the AWS Cloud Development Kit (CDK). It contains a pre-built pipeline that can be deployed to the sandbox account, along with 3 pieces of infrastructure pre-linked as an example to get started shown below.

![provided skeleton code](docs/assets/skeleton.png)

The provided code sets up a publically accessible API using APIGateway that triggers a Lambda function. The message is read and then posted to a SNS notification topic.

### Getting Started & File Overview

This repo has a basic CI pipeline to deploy on the branch configured in `aws_intro/app_config.py`. **Before pushing to bitbucket ensure this file is updated with your branch name!** The pipeline will be setup in the next steps.

* `aws_intro/`: The core application code for the system. Most development takes place in here
* `aws_intro/lambdas/`: This folder contains the source code for the different lambda functions you create
* `aws_intro/app_main_stack.py`: The primary stack for the application. For this workshop infrastructure should be defined here. Additional stacks can be created, but need to be added to the Stage defined in:
* `aws_intro/deployment_pipeline_stack.py`: Contains the code defining the development pipeline and behaviours, as well as the [Stage](https://docs.aws.amazon.com/cdk/api/latest/python/aws_cdk.core/Stage.html) resource that deploys the application in app_main_stack.py
* `app.py`: Triggers the deployment of the development pipeline. In non-pipelined architecture the main application stack can be deployed by instantiating it here
* `resources/`: Collection of scripts not part of the workshop but useful for testing the deployed infrastructure

### Deploying the CI Pipeline

To get started a CodePipeline needs to be created to handle the deployment of resources. This pipeline is pre-configured in the provided code, but **aws_intro/app_config.py** needs to be updated with your branch name.

1. First, run `cdk synth` to ensure the application compiles the templates successfully. You should see a large output of a cloudformation template if it succeeds.
2. **Commit and push your branch to the remote repository before running the deploy command!!!!!!**
3. After the source is verified to be in bitbucket, run `cdk deploy`
4. Review and accept the permission changes the CDK makes: these are the permissions granted to AWS CodePipeline in order to log, build, and deploy the applicaiton.

Your terminal will show the progress of the development pipeline being deployed, and when finished you can login to the [AWS Console](https://axys.awsapps.com/start#/) to see the pipeline under the CodePipeline (or [click here](https://us-west-2.console.aws.amazon.com/codesuite/codepipeline/pipelines?region=us-west-2)).

![codepipeline view](docs/assets/screens/codepipeline.png)

You should see a pipeline created as {USERNAME}-Workshop after a few minutes. This pipeline is setup to self-mutate, so if changes occur it will automatically update its build steps. While normally a pipeline should be fairly consistent, this behaviour allows the addition of new stages for manual approval in moves to production, testing, linting and so on.

Importantly **the manual `cdk deploy` should only ever be run once to create the development pipeline. All future deployments should take place using git commits!** Running `cdk deploy` again will not cause the application code you write to deploy!

### Deploying the Skeleton Application

Once the pipeline has deployed we add a [Stage](https://docs.aws.amazon.com/cdk/api/latest/python/aws_cdk.core/Stage.html) resource to the pipeline. A stage encompasses a group of infrastructure stacks or constructs that comprise the application (in this case just the stack defined in `app_main_stack.py`).

To add the stage, uncomment `pipeline.add_application_stage(DeploymentStage(self, cfg.user, env=cfg.sandbox_env))` in `deployment_pipeline_stack.py`, commit and push.

The {USERNAME}-Workshop pipeline created previously should automatically pickup the code commit and start its update in two phases. First it will perform a *self-mutation* when it sees the addition of the application stage. It will perform the update and you will likely see the pipeline flip to red indicating a build failed. This is normal when updates to the pipeline occur and it restarts to use the updated version. When it reaches the DeploymentStage you will see a AWS CodeBuild instance start the actual work behind synthesizing and deploying the application.

The CDK will first synthesize the code written to generate CloudFormation templates, store them and associated lambda code in an S3 artifacts bucket, and then deploy the finalized stack. It's recommended to open the CloudFormation console and observe the stack as it creates/updates under the Events tab to better understand how the resources are created.

Once the pipeline finishes deploying the application, verify it works by opening the [CloudFormation console](https://us-west-2.console.aws.amazon.com/cloudformation/home?region=us-west-2). You should see two stacks prefixed with your username: {username}-workshop and {username}-WorkshopPipeline. Open the {username-workshop} stack to view details of it including Event history, Resources created, and Outputs. The Outputs include the url for the API Gateway endpoint which when viewed should present a hello world response generated by the demo lambda function.

## Next Steps

Once the initial code is running, building the system shown above is split into 6 different tasks meant to be completed in order.

* [Task 1: Adding a Lambda Function](docs/Task1.md)
* [Task 2: Creating a State Machine](docs/Task2.md)
* [Task 3: Adding DynamoDB](docs/Task3.md)
* [Task 4: More Complicated Step Functions](docs/Task4.md)
* [Task 5: Ingest from a Second Source (SQS Queue)](docs/Task5.md)
* [Task 6: Ingesting From Email](docs/Task6.md)

## Resources

There is a few scripts provided to make testing out ingest to the system. Be aware of the rates that you send at, and I have set some limits in the scripts.

```
python resources/send_sqs.py NUMBER_TO_SEND TIME_BETWEEN_MESSAGES [--SEND_MSG MESSAGE_TEXT] [--SEND_CSV] [--SEND_IMG] [--SEND_BAD_MESSAGE]
python resources/post_to_api.py NUMBER_TO_SEND TIME_BETWEEN_MESSAGES [--SEND_MSG MESSAGE_TEXT] [--SEND_CSV] [--SEND_IMG] [--SEND_BAD_MESSAGE]
```

Some useful resources that may be useful for reference throughout this workshop are provided. (More specific resources are also on the individual task pages.)

| URL                                                                                                 | Description                                                                                                                                                                                                                 |
|-----------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| [Installing WSL](https://docs.microsoft.com/en-us/windows/wsl/install-win10)                        | Install guide for WSL on Windows 10. WSL 2 is not available on AXYS Computers yet.                                                                                                                                          |
| [Developing in WSL with VS Code](https://code.visualstudio.com/docs/remote/wsl)                     | Overview of setting up a development environment in VS Code                                                                                                                                                                 |
| [AWS CDK Constructs](https://docs.aws.amazon.com/cdk/latest/guide/constructs.html)                  | An introduction to the idea of a Construct, the basis for everything in the CDK                                                                                                                                             |
| [AWS CDK API Reference](https://docs.aws.amazon.com/cdk/api/latest/docs/aws-construct-library.html) | The AWS Construct Library API. Typescript version linked here as it contains better examples for some constructs, and the [Python version is available here.](https://docs.aws.amazon.com/cdk/api/latest/python/index.html) |
| [AWS CDK Developer Guide](https://docs.aws.amazon.com/cdk/latest/guide/home.html)                   | Official AWS documentation and introductions to the CDK                                                                                                                                                                     |
| [AWS CDK Workshop](https://cdkworkshop.com/)                                                        | Great resource for getting started and introduced to the AWS CDK.                                                                                                                                                           |

If you need to interface with an AWS Service (be it S3, SNS, Lambda, Step Functions, DynamoDB, ect) from inside Python, [boto3 is your friend](https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/index.html).

## Cleaning up

After completing all tasks, tear down any infrastrucutre setup in AWS. To do so delete the two stacks listed in AWS Cloudformation as `username-WorkshopPipeline` and `username-workshop`.

## About This Repository

The workshop for this project should always be in the `master` branch and should provide only the code outlined in Task 0 above. Solutions for each task will be available on the branch `solution/task#` and can be used as the starting point for task #+1. A full solution will be availalbe in `solution/complete`.

## Changelog

|Date|Author|Description|
|-|-|-|
|Sept 7 2020|Jason Parker|Initial setup|
|Sept 9 2020|Jason Parker|Task Seperation & Skeleton Code|